#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFile"
#include "QTextStream"
#include "QMessageBox"
using namespace std;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    //ingresar
    QFile file("C:\\Users\\kevin\\Downloads\\Concesionario\\Info.txt");
    if(!file.open(QIODevice::WriteOnly)){
        qCritical()<<file.errorString();
    }
    QTextStream stream(&file);
    stream<<"Informacion de los vehiculos"<<"\n";
    stream<<"###############################"<<"\n";
    QString nro=ui->datoNro->text();
    QString placa=ui->datoPlaca->text();
    QString marca=ui->datoMarca->text();
    QString modelo=ui->datoModelo->text();
    QString precio=ui->datoPrecio->text();
    QString codigo=ui->datoCod->text();
    //ui->textEdit->setText("Vehiculo # "+s+'\n'+"Placa: "+placa+'\n'+"Marca: "+marca+'\n'+"Modelo: "+modelo+'\n'+"Precio: "+precio+'\n'+"codigo: "+codigo);
    stream<<"Vehiculo # "<<nro<<"\n";
    stream<<"Placa: "<<placa<<"\n";
    stream<<"Marca: "<<marca<<"\n";
    stream<<"Modelo: "<<modelo<<"\n";
    stream<<"Precio: "<<precio<<"\n";
    stream<<"Codigo Vendedor: "<<codigo<<"\n";
    stream<<"###############################"<<"\n";
    QMessageBox::about(this,"Alerta","Guardado exitoso");
    file.close();
}


void MainWindow::on_pushButton_2_clicked()
{
    //mostrar
    QFile file3("C:\\Users\\kevin\\Downloads\\Concesionario\\Info.txt");
    if (!file3.open(QIODevice::ReadOnly))
        QMessageBox::information(0,"info",file3.errorString());
    QTextStream ins (&file3);
    ui->textEdit->setText(ins.readAll());
}


void MainWindow::on_pushButton_3_clicked()
{
    //Lista
}

